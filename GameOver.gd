extends Node2D

signal again

func setReason(reason):
	$Reason.text = reason

func setKarma(karma):
	$RichTextLabel.text = "Game Over, Karma: " + str(karma) 

func exec():
	$Timer.start()

func _on_Timer_timeout():
	self.visible = true


func _on_Button_pressed():
	emit_signal("again")
