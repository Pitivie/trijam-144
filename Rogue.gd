extends Node2D

var alive = true

func kill():
	if(alive == true):
		$Sprite.rotation_degrees = -90
		self.position.y += 30
		alive = false
