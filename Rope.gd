extends Node2D

signal cut

func _on_TextureButton_pressed():
	emit_signal("cut")
	print("Cut !")
	$TextureButton.margin_top = -125
	$TextureButton.disabled = true
