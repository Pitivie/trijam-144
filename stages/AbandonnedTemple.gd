extends Node2D

signal game_over

func _on_CharacterRope_cut():
	$Character.kill()
	emit_signal("game_over", "Well, you died.")

func finish_stage():
	return 0
