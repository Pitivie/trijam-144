extends Node2D

signal game_over
signal died

func _on_CharacterRope_cut():
	$Character.kill()
	emit_signal("game_over", "Well, you died.")

func finish_stage():
	return 0
	
func _on_OldManRope_cut():
	$Label.text = "The Executioner is disapointed."
	emit_signal("died")

func _on_ExecutionerRope_cut():
	$Label.text = "The old man thanks the gods and the villagers help him to flee."
	emit_signal("died")
