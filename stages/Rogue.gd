extends Node2D

signal game_over
signal died

func _on_CharacterRope_cut():
	$Character.kill()
	emit_signal("game_over", "Well, you died.")

func _on_RogueRope_cut():
	$Label.text = "the rogue drop dead, you can continue your travel"
	$Rogue.kill()
	emit_signal("died")

func finish_stage():
	if($Rogue.alive == true):
		emit_signal("game_over", "you lost the artefact, your failed.")
		return 1
	return 0
