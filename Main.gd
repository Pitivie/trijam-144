extends Node2D

var stages = []
var currentStage
var currentDay = 0
var karma = 10
var isGameOver = false

func _ready():
	stages.push_back(preload("res://stages/Intro.tscn"))
	stages.push_back(preload("res://stages/Rogue.tscn"))
	stages.push_back(preload("res://stages/AbandonnedTemple.tscn"))
	stages.push_back(preload("res://stages/Execution.tscn"))
	stages.push_back(preload("res://stages/EndGame.tscn"))

	newStage()

func _on_Stage_game_over(reason):
	isGameOver = true
	$progressDay.visible = false
	$GameOver.setReason(reason)
	$GameOver.setKarma(karma)
	$GameOver.exec()

func _on_Stage_died():
	karma -=1
 
func _on_FinishDay_pressed():
	if(currentStage.finish_stage() == 0): 
		nextStage()

func nextStage():
	currentDay += 1
	$Stage.remove_child(currentStage)
	newStage()

func newStage():
	currentStage = stages[currentDay].instance()
	currentStage.connect("game_over", self, "_on_Stage_game_over")
	currentStage.connect("died", self, "_on_Stage_died")
	$Stage.add_child(currentStage)
	if(currentStage.get_name() == "EndGame"):
		$Timer.stop()
		$progressDay.visible = false
		$FinishDay.visible = false
	$progressDay.value = 100

func _on_Timer_timeout():
	$progressDay.value -= 10
	if($progressDay.value == 0 && isGameOver == false):
		if(currentStage.finish_stage() == 0): 
			nextStage()
